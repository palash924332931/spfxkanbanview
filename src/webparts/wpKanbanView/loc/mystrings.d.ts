declare interface IWpKanbanViewWebPartStrings {
  PropertyPaneDescription: string;
  BasicGroupName: string;
  DescriptionFieldLabel: string;
}

declare module 'WpKanbanViewWebPartStrings' {
  const strings: IWpKanbanViewWebPartStrings;
  export = strings;
}
